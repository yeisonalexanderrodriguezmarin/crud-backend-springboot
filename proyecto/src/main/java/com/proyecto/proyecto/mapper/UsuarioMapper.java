package com.proyecto.proyecto.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.proyecto.proyecto.dto.UsuarioDTO;
import com.proyecto.proyecto.dto.request.UsuarioRequest;
import com.proyecto.proyecto.response.ConsultarUsuarioResponse;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE) 
public interface UsuarioMapper {

	UsuarioDTO crearUsuarioRequestToDTO(UsuarioRequest request);
	
	List<ConsultarUsuarioResponse> consultarUsuarioDtoToResponse(List<UsuarioDTO> response);
	
	List<ConsultarUsuarioResponse> consultarUsuarioPorIdDtoToResponse(List<UsuarioDTO> response);

	UsuarioDTO actualizarUsuarioRequestToDTO(UsuarioRequest request);
	
}
