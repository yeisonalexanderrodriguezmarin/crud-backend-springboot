package com.proyecto.proyecto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.proyecto.proyecto.dto.request.UsuarioRequest;
import com.proyecto.proyecto.response.ConsultarUsuarioResponse;
import com.proyecto.proyecto.response.MensajeResponse;
import com.proyecto.proyecto.service.UsuarioService;

@CrossOrigin("*")
@RestController // http://localhost:8080/crud
@RequestMapping(value = "/crud")
public class UsuarioController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@GetMapping(value = "/consultar-usuarios")
	public ResponseEntity<List<ConsultarUsuarioResponse>> consultarUsuarios() throws Exception {
		return new ResponseEntity<>(usuarioService.consultarUsuarios(), HttpStatus.OK);
	}
	
	@GetMapping(value = "/consultar-por-id")
	public ResponseEntity<List<ConsultarUsuarioResponse>> consultarPorId(@RequestParam Long usuarioId) throws Exception {
		return new ResponseEntity<>(usuarioService.consultarPorId(usuarioId), HttpStatus.OK);
	}

	@PostMapping(value = "/crear-usuarios")
	public ResponseEntity<MensajeResponse> crearUsuarios(@RequestBody UsuarioRequest request) throws Exception {
		return new ResponseEntity<>(usuarioService.crearUsuarios(request), HttpStatus.OK);
	}
	
	@PutMapping(value = "/actualizar-usuarios")
	public ResponseEntity<MensajeResponse> actualizarUsuarios(@RequestBody UsuarioRequest request) throws Exception {
		return new ResponseEntity<>(usuarioService.actualizarUsuarios(request), HttpStatus.OK);
	}
	
	@DeleteMapping("/eliminar-usuario")
	public ResponseEntity<MensajeResponse> eliminarUsuario(@RequestParam Long usuarioId) 
			throws Exception {
				return new ResponseEntity<>(usuarioService.eliminarUsuario(usuarioId), HttpStatus.OK);
	}
	
}

	
