package com.proyecto.proyecto.repository;

import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import com.proyecto.proyecto.dto.UsuarioDTO;

public interface UsuarioDAO {

	void crearUsuarios(UsuarioDTO usuario) throws Exception;
	
	void actualizarUsuarios(UsuarioDTO usuario) throws Exception;
	
	List<UsuarioDTO> consultarUsuarios() throws Exception;
	
	List<UsuarioDTO> consultarPorId(Long usuarioId) throws Exception;
	
	void eliminarUsuario(@RequestParam Long usuarioId) throws Exception;
	
}
