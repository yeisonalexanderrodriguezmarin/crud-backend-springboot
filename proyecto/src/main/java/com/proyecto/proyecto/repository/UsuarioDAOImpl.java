package com.proyecto.proyecto.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.proyecto.proyecto.dto.UsuarioDTO;

@Repository
public class UsuarioDAOImpl implements UsuarioDAO{
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public void crearUsuarios(UsuarioDTO usuario) throws Exception {
		try {
			
//			Long usuarioId = 5L;
			
//			usuario.setUsuarioId(usuarioId);
			
			/*if (usuario.getUsuarioId() == null) {
	            throw new Exception("El campo USUARIO_ID no puede ser nulo");
	        }*/
			
			String sql = "INSERT INTO USUARIOS ("
					+ "NOMBRE, "
					+ "APELLIDO, "
					+ "CORREO"
					+ ") "
					+ "VALUES (?, ?, ?)";
			
			jdbcTemplate.update(sql,
					usuario.getNombre(),
					usuario.getApellido(),
					usuario.getCorreo()
					);
		} catch (Exception e) {
//			 e.printStackTrace();
		        throw new Exception("Error al crear un usuario");
		}
	}

	@Override
	public void actualizarUsuarios(UsuarioDTO usuario) throws Exception {
		try {
			
			String sql = "UPDATE USUARIOS SET "
					+ "NOMBRE = ?, "
					+ "APELLIDO = ?, "
					+ "CORREO = ? "
					+ "WHERE USUARIO_ID = ?";
			jdbcTemplate.update(sql, 
					usuario.getNombre(),
					usuario.getApellido(),
					usuario.getCorreo(),
					usuario.getUsuarioId()
					);
		} catch (Exception e) {
			throw new Exception("Error al editar el usuario");
		}
	}
	
	public List<UsuarioDTO> consultarUsuarios() throws Exception {
	    try {
	        String query = "SELECT * FROM USUARIOS";
	        return jdbcTemplate.query(query, new UsuariosRowMapers());
	    } catch (Exception e) {
	        // Imprimir o loggear la excepción original
//	        e.printStackTrace();
	        throw new Exception("Error al consultar usuarios");
	    }
	}
	
	// consultar por id
	@Override
	public List<UsuarioDTO> consultarPorId(Long usuarioId) throws Exception {
		try {
			String sql = "SELECT * FROM USUARIOS WHERE USUARIO_ID = ?";
			return jdbcTemplate.query(sql, new UsuariosRowMapers(), usuarioId);
		} catch (Exception e) {
			throw new Exception("Error al consultar por id");
		}
	}
	
	@Override
	public void eliminarUsuario(Long usuarioId) throws Exception {
		try {
			String sql = "DELETE FROM USUARIOS WHERE USUARIO_ID = ? ";
			jdbcTemplate.update(sql, usuarioId);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error al eliminar un usuario: ");
		} 
	}
	
	private static class UsuariosRowMapers implements org.springframework.jdbc.core.RowMapper<UsuarioDTO> {

		@Override
		public UsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			UsuarioDTO usuario = new UsuarioDTO();
			
			usuario.setUsuarioId(rs.getLong("USUARIO_ID"));
			usuario.setNombre(rs.getString("NOMBRE"));
			usuario.setApellido(rs.getString("APELLIDO"));
			usuario.setCorreo(rs.getString("CORREO"));
			
			return usuario;
			
		}
		
	}
	
}
