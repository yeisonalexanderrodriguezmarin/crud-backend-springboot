package com.proyecto.proyecto.response;

public class MensajeResponse {

	private String mensaje;

	public MensajeResponse() {
		super();
	}

	public MensajeResponse(String mensaje) {
		super();
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
}
