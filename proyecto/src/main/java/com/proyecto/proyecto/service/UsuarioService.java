package com.proyecto.proyecto.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import com.proyecto.proyecto.dto.request.UsuarioRequest;
import com.proyecto.proyecto.response.ConsultarUsuarioResponse;
import com.proyecto.proyecto.response.MensajeResponse;

public interface UsuarioService {

	MensajeResponse crearUsuarios(UsuarioRequest request) throws Exception;

	List<ConsultarUsuarioResponse> consultarUsuarios() throws Exception;

	List<ConsultarUsuarioResponse> consultarPorId(Long usuarioId) throws Exception;
	
	MensajeResponse actualizarUsuarios(UsuarioRequest request) throws Exception;
	
	MensajeResponse eliminarUsuario(@RequestParam Long usuarioId) throws Exception;

}
