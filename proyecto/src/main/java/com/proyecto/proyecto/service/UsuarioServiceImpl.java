package com.proyecto.proyecto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.proyecto.dto.UsuarioDTO;
import com.proyecto.proyecto.dto.request.UsuarioRequest;
import com.proyecto.proyecto.mapper.UsuarioMapper;
import com.proyecto.proyecto.repository.UsuarioDAO;
import com.proyecto.proyecto.response.ConsultarUsuarioResponse;
import com.proyecto.proyecto.response.MensajeResponse;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioDAO usuarioDAO;

	@Autowired
	UsuarioMapper mapper;
	
	@Override
	public MensajeResponse crearUsuarios(UsuarioRequest request) throws Exception {
		
		try {
			UsuarioDTO usuarioDTO = mapper.crearUsuarioRequestToDTO(request);
			
			usuarioDAO.crearUsuarios(usuarioDTO);
			
			MensajeResponse mensaje = new MensajeResponse();
			
			mensaje.setMensaje("Usuario creado exitosamente");
			
			return mensaje;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
	}
	
	@Override
	public MensajeResponse actualizarUsuarios(UsuarioRequest request) throws Exception {
		
		try {
			UsuarioDTO usuarioDTO = mapper.actualizarUsuarioRequestToDTO(request);
			
			usuarioDAO.actualizarUsuarios(usuarioDTO);
			
			MensajeResponse mensaje = new MensajeResponse();
			
			mensaje.setMensaje("Usuario editado exitosamente");
			
			return mensaje;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
	}

	@Override
	public List<ConsultarUsuarioResponse> consultarUsuarios() throws Exception {
		try {
			
			List<ConsultarUsuarioResponse> consultar = mapper.consultarUsuarioDtoToResponse(
					usuarioDAO.consultarUsuarios()
					);
			
			return consultar;
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	@Override
	public List<ConsultarUsuarioResponse> consultarPorId(Long usuarioId) throws Exception {
		try {
			List<ConsultarUsuarioResponse> consultar = mapper.consultarUsuarioPorIdDtoToResponse(
					usuarioDAO.consultarPorId(usuarioId));
			
			return consultar;
		
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public MensajeResponse eliminarUsuario(Long usuarioId) throws Exception {
		try {
			
			usuarioDAO.eliminarUsuario(usuarioId);
			
			MensajeResponse mensaje = new MensajeResponse();
			
			mensaje.setMensaje("Se elimino el usuario correctamente!!");

			return mensaje;
			
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
		
	}

}
