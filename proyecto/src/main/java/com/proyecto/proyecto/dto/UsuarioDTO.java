package com.proyecto.proyecto.dto;

public class UsuarioDTO {

	private Long usuarioId;
	
	private String nombre;
	
	private String apellido;
	
	private String correo;

	public UsuarioDTO() {
		super();
	}

	public UsuarioDTO(Long usuarioId, String nombre, String apellido, String correo) {
		super();
		this.usuarioId = usuarioId;
		this.nombre = nombre;
		this.apellido = apellido;
		this.correo = correo;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
}
